/**
 * 
 */
package ru.otus.mvcsecurity.repostory;

import org.springframework.data.repository.CrudRepository;

import ru.otus.mvcsecurity.domain.NegativeActivity;

/**
 * @author feodor
 *
 */
public interface NegativeActivityRepository extends CrudRepository<NegativeActivity, Long> {

}
