--
-- Файл сгенерирован с помощью SQLiteStudio v3.3.3 в ср февр. 2 15:54:16 2022
--
-- Использованная кодировка текста: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Таблица: Classification
CREATE TABLE Classification ("ID classification" INTEGER NOT NULL PRIMARY KEY, "Name classification" TEXT);

-- Таблица: History
CREATE TABLE History ("ID resident" INTEGER UNIQUE REFERENCES Resident ("ID Resident"), "ID history" INTEGER UNIQUE NOT NULL, Date TEXT, Event TEXT NOT NULL, "Who introduced" TEXT NOT NULL);

-- Таблица: Negative activity
CREATE TABLE "Negative activity" ("ID violations" INTEGER PRIMARY KEY NOT NULL UNIQUE, "ID resident" INTEGER REFERENCES Resident ("ID Resident") NOT NULL, "ID period" INTEGER REFERENCES "Table period" ("ID period") NOT NULL, "Type of violation" TEXT, "Number of points" INTEGER, "Date of violation" TEXT, "Justifying document" TEXT, "Type of punishment" TEXT, Comment TEXT, "Respectful explanatory" TEXT, "Not respectful explanatory" TEXT, "Date introduced" TEXT NOT NULL, "Who introduced" TEXT NOT NULL, "Comment who introduced" TEXT NOT NULL, "ID classification" INTEGER NOT NULL REFERENCES Classification ("ID classification"));

-- Таблица: Positive activity
CREATE TABLE "Positive activity" ("ID activiy" INTEGER UNIQUE NOT NULL, Comment TEXT NOT NULL, "ID resident" INTEGER REFERENCES Resident ("ID Resident") NOT NULL, "Who introduced" TEXT NOT NULL, "date activity" TEXT);

-- Таблица: Presence calendar
CREATE TABLE "Presence calendar" ("ID resident" INTEGER REFERENCES Resident ("ID Resident") UNIQUE NOT NULL, "Type of absence" BOOLEAN NOT NULL, "Start date" TEXT, "End date" TEXT, Weekends BOOLEAN, Address TEXT NOT NULL, "Start date of the week" TEXT, "The presence of a petition" BOOLEAN, "Time of delay" TEXT);

-- Таблица: Resident
CREATE TABLE Resident ("ID Resident" INTEGER PRIMARY KEY UNIQUE NOT NULL, Course INTEGER NOT NULL, Faculty TEXT NOT NULL, "Learning conditions" TEXT NOT NULL, Direction TEXT NOT NULL, "Group" TEXT NOT NULL, "Education level" TEXT NOT NULL, Citizenship TEXT NOT NULL, "Date of bitrh" TEXT NOT NULL, "Month of birth" INTEGER NOT NULL, "Year of birth" INTEGER NOT NULL, Gender BOOLEAN NOT NULL, "Telephon number" TEXT, "Points per semester" INTEGER, FIO TEXT NOT NULL, "Room number" INTEGER NOT NULL);

-- Таблица: Rooms
CREATE TABLE Rooms ("ID resident" INTEGER REFERENCES Resident ("ID Resident"), "ID rooms " INTEGER UNIQUE PRIMARY KEY NOT NULL, "Rooms content" INTEGER NOT NULL, "Number rooms" INTEGER NOT NULL);

-- Таблица: Subsidy
CREATE TABLE Subsidy ("ID subsidy" INTEGER PRIMARY KEY NOT NULL UNIQUE, Date TEXT, "Start date subsidy" TEXT, "End date subsidy" TEXT, "Who introduced" TEXT, "Summ subsidy" INTEGER, Comment TEXT, "ID resident" INTEGER REFERENCES Resident ("ID Resident") NOT NULL);

-- Таблица: Table period
CREATE TABLE "Table period" ("ID period" INTEGER PRIMARY KEY, "Name period" TEXT);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
