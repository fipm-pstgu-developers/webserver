package ru.otus.mvcsecurity.rest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.time.temporal.ChronoUnit;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ru.otus.mvcsecurity.domain.Periods;
import ru.otus.mvcsecurity.domain.PresenceCalendar;
import ru.otus.mvcsecurity.domain.Resident;
import ru.otus.mvcsecurity.domain.Violations;
import ru.otus.mvcsecurity.repostory.HolidaysRepository;
import ru.otus.mvcsecurity.repostory.PeriodsRepository;
import ru.otus.mvcsecurity.repostory.PresenceCalendarRepository;
import ru.otus.mvcsecurity.repostory.ResidentRepository;
import ru.otus.mvcsecurity.repostory.ViolationsRepository;

@Controller
public class PresenceCalendarController {

	@Autowired
	private ResidentRepository resRepo;

	@Autowired
	private PresenceCalendarRepository repository;

	@Autowired
	private ViolationsRepository vioRepo;

	@Autowired
	private HolidaysRepository repHol;

	@Autowired
	private PeriodsRepository perRepo;

	/**
	 * Метод addPresenceCalendar должен добавлять запись в книгу прошений. Нужен
	 * для фиксации отсутствия насельника в общежитии. Метод form служит для
	 * функционирования Get запроса и соответственно Post метода
	 * addPresenceCalendar.
	 * 
	 * 
	 * @param fio             - ФИО насельника
	 * @param course          - курс насельника
	 * @param faculty         - факультет насельника
	 * @param weekends        - параметр, отвечающий за пропуски студента в
	 *                        выходные: пт/сб, сб/вс + праздники (двунадесятые и
	 *                        гос.)
	 * @param address         - адрес, по которому уезжает насельник
	 * @param startDateAbsent - начало отьезда насельника вне выходных или на
	 *                        каникулы
	 * @param endDateAbsent   - приезд насельника в общежитие с выходных/каникул
	 * @param timeDelay       - время опоздания в общежитие насельника (в этот день
	 *                        насельник есть в общежитие, но опаздывает и приходит
	 *                        после комендантского часа)
	 * @param model           - параметр нужен для связи формы и метода контроллера
	 * @return presence_calendar - возвращает шаблон в браузер, с помощью которого
	 *         мы забираем данные в БД
	 */
	@GetMapping("/PresenceCalendar")
	public String form() {
		return "presence_calendar";
	}

	@PostMapping("/PresenceCalendar")
	public String addPresenceCalendar(@RequestParam String fio, @RequestParam Integer course,
			@RequestParam String faculty, @RequestParam(required = false) boolean weekends,
			@RequestParam String address, @RequestParam(required = false) String startDateAbsent,
			@RequestParam(required = false) String endDateAbsent, @RequestParam String dateAbsent,
			@RequestParam(required = false) String timeDelay, @RequestParam String reasonAbsence) {

		Iterable<Resident> ress = resRepo.findByFioContainsAndCourse(fio, course);
		List<LocalDate> daysBetween;
		Long idRes;
		/**
		 * Дата записи/подачи прошения записывается автоматически за актуальную
		 * системную дату, пользователь не вводит ее. Важное уточнение: после 12 уже
		 * прошение запишется как за след день.
		 */
		LocalDate startDateWeek = LocalDate.now();

		/**
		 * Если несколько насельников, то на каждого насельника вносится прошение, но
		 * подразумевается, что найдется лишь один насельник
		 */
		for (Resident resident2 : ress) {
			idRes = resident2.getIdResident();

			if (!startDateAbsent.isEmpty() && !endDateAbsent.isEmpty()) {
				daysBetween = getDatesBetween(LocalDate.parse(startDateAbsent), LocalDate.parse(endDateAbsent));

				for (LocalDate localDate : daysBetween) {
					PresenceCalendar presenceCalendar = new PresenceCalendar();
					presenceCalendar.setIdResident(idRes);
					presenceCalendar.setDateAbsent(localDate);
					if (!address.isEmpty()) {
						presenceCalendar.setAddress(address);
					}
					presenceCalendar.setStartDateWeek(startDateWeek);
					repository.save(presenceCalendar);
				}
			}

			/**
			 * Если прошение подается на период, то каждую дату периода записываем отдельно
			 * в календаре присутствия
			 */
			if (weekends == true) {
				PresenceCalendar presenceCalendar = new PresenceCalendar();
				presenceCalendar.setIdResident(idRes);
				presenceCalendar.setWeekends(weekends);
				presenceCalendar.setStartDateWeek(startDateWeek);
				presenceCalendar.setAddress(address);
				repository.save(presenceCalendar);
			}
			if (!dateAbsent.isEmpty()) {
				PresenceCalendar presenceCalendar = new PresenceCalendar();
				presenceCalendar.setIdResident(idRes);
				presenceCalendar.setDateAbsent(LocalDate.parse(dateAbsent));
				presenceCalendar.setStartDateWeek(startDateWeek);
				presenceCalendar.setAddress(address);
				repository.save(presenceCalendar);
			}
			/**
			 * Внимание: Разделяем по подстроке и соединяем в единую строку, чтобы
			 * избавиться от символа T, который мешает парсингу даты в БД.
			 */
			// TODO
			if (!timeDelay.isEmpty()) {
				PresenceCalendar presenceCalendar = new PresenceCalendar();
				presenceCalendar.setIdResident(idRes);
				timeDelay = timeDelay.substring(0, 10) + " " + timeDelay.substring(11, 16);
				System.out.println("timeDelay" + timeDelay);
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
				presenceCalendar.setTimeDelay(LocalDateTime.parse(timeDelay, formatter));

				if (!reasonAbsence.isEmpty()) {
					presenceCalendar.setReasonAbsence(reasonAbsence);
				}
				presenceCalendar.setStartDateWeek(startDateWeek);
				repository.save(presenceCalendar);

			}
		}
		return "presence_calendar";
	}

	/**
	 * Метод, принимающая из формы дату, за которую нужно показать поданные
	 * прошения.
	 * 
	 * @param startDateWeek - дата, за которую ищем поданные прошения.
	 * @return шаблон для отрисовки
	 */
	@GetMapping("/presenceCalendarViewDate")
	public String formView() {
		return "presence_calendar_view_date";
	}

	@PostMapping("/presenceCalendarViewDate")
	public String viewPresenceCalendar(@RequestParam String startDateWeek, Model model) {

		LocalDateTime timeD;
		Resident resident = new Resident();
		List<Resident> resList = new ArrayList();
		if (!startDateWeek.isEmpty()) {
			Iterable<PresenceCalendar> prCalendar = repository.findByStartDateWeek(LocalDate.parse(startDateWeek));
			for (PresenceCalendar presenceCalendar : prCalendar) {
				resident = resRepo.findById(presenceCalendar.getIdResident()/* (long) 512 */).get();/* ? */
				resList.add(resident);

			}
			model.addAttribute("prCalendar", prCalendar);
		}

		model.addAttribute("date2", startDateWeek);

		model.addAttribute("resident", resList);

		return "presence_calendar_view_date";
	}

	/**
	 * Отображает записанные прошения в БД с помощью КП(книги прошений) за период.
	 * 
	 * @param startDate - начало периода, начиная с которого начинаем искать
	 * @param endDate   - конец периода, с начиная с которого начинаем искать
	 * @return шаблон для отрисовки
	 */
	@GetMapping("/presenceCalendarViewPeriods")
	public String formViewPeriods() {
		return "presence_calendar_view_periods";
	}

	@PostMapping("/presenceCalendarViewPeriods")
	public String viewPresenceCalendarPeriods(@RequestParam String startDate, @RequestParam String endDate,
			Model model) {

		Resident resident = new Resident();
		List<Resident> resList = new ArrayList();
		List<PresenceCalendar> presenceList = new ArrayList();

		List<LocalDate> daysBetween = getDatesBetween(LocalDate.parse(startDate), LocalDate.parse(endDate));
		/**
		 * В цикле берем каждую дату из введеного промежутка и по каждой дате выполняем
		 * запрос из БД, забирая по каждой дате прошения.
		 */
		for (LocalDate localDate : daysBetween) {
			System.out.println("localDate " + localDate);
			Iterable<PresenceCalendar> prCalendar = repository.findByStartDateWeek(localDate);

			for (PresenceCalendar presenceCalendar : prCalendar) {
				resident = resRepo.findById(presenceCalendar.getIdResident()/* (long) 512 */).get();/* ? */
				resList.add(resident);
				presenceList.add(presenceCalendar);
				System.out.println("resident " + resident.getFio());
			}
		}
		model.addAttribute("prCalendar", presenceList);
		model.addAttribute("resident", resList);
		model.addAttribute("date3", startDate);
		model.addAttribute("date4", endDate);

		return "presence_calendar_view_periods";
	}

	public static List<LocalDate> getDatesBetween(LocalDate startDate, LocalDate endDate) {
		long numOfDaysBetween = ChronoUnit.DAYS.between(startDate, endDate.plusDays(1));
		return IntStream.iterate(0, i -> i + 1).limit(numOfDaysBetween).mapToObj(i -> startDate.plusDays(i))
				.collect(Collectors.toList());
	}

	/**
	 * Форма отметки администратора. Автоматически коррелирует поданные прошения на
	 * отсутствия и нарушения при отсутствии насельника в общежитии.
	 * 
	 * @param model - контейнер для шабона
	 * @return форму для отрисовки со всеми насельниками
	 */

	@GetMapping("/presenceCalendarAdmin")
	public String getFormAdmin(Model model) {
		List<Resident> r = (List<Resident>) resRepo.findAll();
		model.addAttribute("residentList", r);
//		boolean presence[] = new boolean[r.size()];
//		Arrays.fill(presence, false);
//		for (boolean b : presence) {
//			System.out.println("presence: " + b);
//		}
//		model.addAttribute("presenceList", presence);
//		
		return "admin_log";
	}

	@PostMapping("/presenceCalendarAdmin")
	public String viewPresenceCalendarAdmin(@RequestParam String dateWeek, @RequestParam("student") String[] student,
			Model model) {

		LocalDate dateW = LocalDate.parse(dateWeek);
		PresenceCalendar prCalendar;
		ArrayList<Long> residentListId = new ArrayList<>();
		Violations vio;

		int i = 0;
		while (i < student.length) {
			if (i + 1 < student.length) {
				if (!student[i + 1].equals("was")) {
					System.out.println("student[i]" + i + student[i]);
					residentListId.add(Integer.toUnsignedLong(Integer.parseInt(student[i])));
				} else {
					i++;
				}
			}
			i++;
		}
		;
		;
		/**
		 * Если человек отсутствует в общежитии.
		 * repository.findByDateAbsentAndIdResident(dateW, resident.getIdResident()) -
		 * поиск, есть ли у человека по данной дате прошение.
		 * repHol.findByDateHoliday(dateW) - проверка, есть ли в БД информация на данный
		 * день как о праздничном. repository.findByWeekends(resident.getIdResident()) -
		 * проверка, есть ли в БД у человека прошение на выходные/праздники
		 * 
		 */
		for (Long resId : residentListId) {
			vio = new Violations();
			prCalendar = new PresenceCalendar();

			System.out.println("CODE WORKING");
			System.out.println("residentId: " + resId);

			/**
			 * Проверяем, есть ли у человека прошение на сегодняшнюю дату. Если есть -
			 * ставим отметку в КП.
			 */
			if (repository.findByIdResidentAndDateAbsent(resId, dateW).isPresent()) {
				System.out.println("БЛОК ПРОСТАНОВКИ ПРОШЕНИЯ ЗА ОТСУТСТВИЯ");
				prCalendar = repository.findByIdResidentAndDateAbsent(resId, dateW).get();
				prCalendar.setPetitionUsed(true);
				repository.save(prCalendar);
			}
			/**
			 * Прошения нет. Проверяем, праздник ли сегодня.
			 */
			else {
				System.out.println("БЛОК ПРОВЕРКИ ПРАЗДНИК ЛИ СЕГОДНЯ");

				/**
				 * Если праздник - проверяем, есть ли прошение на выходные/праздники
				 */
				if (repHol.findByDateHoliday(dateW).isPresent()) {

					System.out.println("БЛОК ПРОВЕРКИ, ЕСТЬ ЛИ ПРОШЕНИЕ НА ВЫХОДНЫЕ");
					/**
					 * Проверяем, есть ли прошение на выходные.
					 */

					if (repository.findByWeekendsAndIdResident(true, resId).isPresent()) {
						System.out.println("ПРОСТАНОВКА ПРОШЕНИЯ");
						prCalendar = repository.findByIdResidentAndDateAbsent(resId, dateW).get();
						prCalendar.setPetitionUsed(true);
						prCalendar.setWeekends(true);
						repository.save(prCalendar);
					}
					/**
					 * Блок, отвечающий за запись нарушения в БД за пришедшую из формы дату.
					 */
					else {
						System.out.println("ПРОСТАНОВКА НАРУШЕНИЯ");
						vio.setIdResident(resId);
						vio.setIdPeriod(gettingIntoPeriod(dateW).getIdPeriod());
						vio.setPointsNumber(2);
						vio.setDateViolations(dateW);
						vio.setCommentViolations("Отсутствовал " + dateW);
						vioRepo.save(vio);
					}

				}
				/**
				 * Если это не праздник, проверяем, не выходной ли сегодня c помощью определения
				 * цифры дня. Так, пятница - 5, сб - 6, вск - 7.
				 */
				else {
					System.out.println("ПРОВЕРКА, ВЫХОДНОЙ ЛИ СЕГОДНЯ ДЕНЬ");
					/**
					 * Проверяем, день отсутствия - выходной(пт/сб/вск) ?.
					 */
					if ((dateW.getDayOfWeek().getValue()) == 5 || (dateW.getDayOfWeek().getValue()) == 6
							|| (dateW.getDayOfWeek().getValue()) == 7) {
						System.out.println("ПРОВЕРКА ЕСТЬ ЛИ ПРОШЕНИЕ НА ВЫХОДНЫЕ");
						/**
						 * Проверяем, есть ли прошение на выходные.
						 */
						if (repository.findByWeekendsAndIdResident(true, resId).isPresent()) {
							System.out.println("ПРОВЕРКА, КАКОЙ СЕГОДНЯ ВЫХОДНОЙ ДЕНЬ");
							switch (dateW.getDayOfWeek().getValue()) {
							case 5:
								if (repository.findByDateAbsent(dateW)) {

								} else {
									prCalendar.setIdResident(resId);
									prCalendar.setDateAbsent(dateW);
									prCalendar.setAddress("Домой");
									prCalendar.setStartDateWeek(LocalDate.now());
									prCalendar.setPetitionUsed(true);
									prCalendar.setWeekends(true);
									repository.save(prCalendar);
								}
								System.out.println("ЗАПИСЬ ПРОШЕНИЯ");

								break;

							case 6:
								if (repository.findByDateAbsent(dateW)) {

								} else {
									prCalendar.setIdResident(resId);
									prCalendar.setDateAbsent(dateW);
									prCalendar.setAddress("Домой");
									prCalendar.setStartDateWeek(LocalDate.now());
									prCalendar.setPetitionUsed(true);
									prCalendar.setWeekends(true);
									repository.save(prCalendar);
								}
								System.out.println("ЗАПИСЬ ПРОШЕНИЯ");

								break;

							case 7:

								if (repository.findByIdResidentAndDateAbsent(resId, dateW.minusDays(1)).isPresent()
										&& repository.findByIdResidentAndDateAbsent(resId, dateW.minusDays(2))
												.isPresent()) {
									vio.setIdResident(resId);
									vio.setIdPeriod(gettingIntoPeriod(dateW).getIdPeriod());
									vio.setPointsNumber(2);
									vio.setDateViolations(dateW);
									vio.setCommentViolations("Отсутствовал " + dateW);
									vioRepo.save(vio);
								} else {
									if (repository.findByDateAbsent(dateW)) {

									} else {
										prCalendar.setIdResident(resId);
										prCalendar.setDateAbsent(dateW);
										prCalendar.setAddress("Домой");
										prCalendar.setStartDateWeek(LocalDate.now());
										prCalendar.setPetitionUsed(true);
										prCalendar.setWeekends(true);
										repository.save(prCalendar);
									}
									System.out.println("ЗАПИСЬ ПРОШЕНИЯ");

								}
								break;

							default:

								break;
							}
						}
						/**
						 * Нет прошения на выходные - записываем нарушение
						 */
						else {
							System.out.println("ЗАПИСЬ НАРУШЕНИЯ");
							vio.setIdResident(resId);
							vio.setIdPeriod(gettingIntoPeriod(dateW).getIdPeriod());
							vio.setPointsNumber(2);
							vio.setDateViolations(dateW);
							vio.setCommentViolations("Отсутствовал " + dateW);
							vioRepo.save(vio);
						}

					}
					/**
					 * Если не выходной, то записываем нарушение
					 */
					else {
						System.out.println("ЗАПИСЬ НАРУШЕНИЯ");
						vio.setIdResident(resId);
						vio.setIdPeriod(gettingIntoPeriod(dateW).getIdPeriod());
						vio.setPointsNumber(2);
						vio.setDateViolations(dateW);
						vio.setCommentViolations("Отсутствовал " + dateW);
						vioRepo.save(vio);
					}

				}

			}
		}
		return "redirect:/presenceCalendarAdmin";
	}

	public Periods gettingIntoPeriod(LocalDate dateAbsent) {

		Long id = null;
		Periods per = new Periods();

		/**
		 * 1) Вызвать все объекты таблицы периодов
		 */
		/**
		 * 2) Через итерацию у каждого объекта взять начало и конец периода
		 */
		for (Periods periods : perRepo.findAll()) {

			/**
			 * 3) Передать начало и конец периода в метод, считающий разницу дней этих
			 * периодов
			 * 
			 */
			List<LocalDate> daysBetween = getDatesBetween(periods.getStartDatePeriod(), periods.getEndDatePeriod());

			/**
			 * 5) Если на каком - то этапе итерации сравнение выдает true, то выходим из
			 * циклов и по промежуткам, которые ввели для измерения разницы, определяем айди
			 * периода 6) после определения айди, передаем этот айди в имени нашего метода,
			 * чтобы внести этот айди в требующуюся таблицу.
			 */
			if (daysBetween.contains(dateAbsent)) {
				per = periods;
				break;
			}

		}
//		TODO - нужно ли возвращать periods таким образом?
		return per;
	}

	/**
	 * Форма заполнения опозданий насельников.
	 * 
	 * @param model - контейнер для шаблона
	 * @return шаблон для отрисовки формы с заполненными насельниками
	 */
	@GetMapping("/delay")
	public String getFormDelay(Model model) {
		List<Resident> r = (List<Resident>) resRepo.findAll();
		model.addAttribute("residentList", r);
		return "book_delay";
	}

	@PostMapping("/delay")
	public String viewPresenceCalendarDelay(@RequestParam String dateWeek, @RequestParam("student") String[] student,
			Model model) {

		LocalDate dateW = LocalDate.parse(dateWeek);
		PresenceCalendar prCalendar;
		ArrayList<Long> residentListId = new ArrayList<>();
		Violations vio;
		ArrayList<LocalDateTime> dateDelay = new ArrayList<>();

		for (String stud : student) {
			System.out.println("stud: " + stud);
		}
//		int i = 0;
//		while (i < student.length) {
//			if (i + 1 < student.length) {
//				if (student[i + 1] != null) {
//					System.out.println("student[i]" + i + student[i]);
//					residentListId.add(Integer.toUnsignedLong(Integer.parseInt(student[i])));
//					
//				} else {
//					i++;
//				}
//			}
//			i++;
//		}
//		;
//		;

		return "redirect:/delay";
	}
}
