
package ru.otus.mvcsecurity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.Transactional;

import ru.otus.mvcsecurity.domain.PresenceCalendar;
import ru.otus.mvcsecurity.domain.Resident;
import ru.otus.mvcsecurity.repostory.PresenceCalendarRepository;
import ru.otus.mvcsecurity.repostory.ResidentRepository;

import javax.annotation.PostConstruct;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
//import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
//import org.o7planning.apachepoiexcel.model.Employee;
//import org.o7planning.apachepoiexcel.model.EmployeeDAO;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.Date;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.boot.context.embedded.MultipartConfigFactory;
import javax.servlet.MultipartConfigElement;

//@Configuration
//@ComponentScan
//@EnableAutoConfiguration	
@SpringBootApplication
public class MvcSecurityApplication {
	
//	 @Bean
//	    public MultipartConfigElement multipartConfigElement() {
//	        MultipartConfigFactory factory = new MultipartConfigFactory();
//	        factory.setMaxFileSize("128KB");
//	        factory.setMaxRequestSize("128KB");
//	        return factory.createMultipartConfig();
//	    }
	
	
	public static void main(String[] args) {

		SpringApplication.run(MvcSecurityApplication.class, args);
		
	}

	@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
	@Autowired
	private PresenceCalendarRepository repository;
	
	@Autowired
	public ResidentRepository residentRepo;


	

	@PostConstruct
	public void init() {
//        repository.save(new PresenceCalendar("Pushkin"));
//        repository.save(new PresenceCalendar("Lermontov"));
//        repository.save(new PresenceCalendar("Esenin"));
//        repository.save(new PresenceCalendar("Tolstoy"));
	}

}
