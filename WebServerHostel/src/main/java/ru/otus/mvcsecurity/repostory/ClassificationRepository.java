/**
 * 
 */
package ru.otus.mvcsecurity.repostory;

import org.springframework.data.repository.CrudRepository;

import ru.otus.mvcsecurity.domain.Classification;

/**
 * @author feodor
 *
 */
public interface ClassificationRepository extends CrudRepository<Classification, Long> {

}
