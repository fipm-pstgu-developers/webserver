package ru.otus.mvcsecurity.repostory;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ru.otus.mvcsecurity.domain.PresenceCalendar;

@Repository
public interface PresenceCalendarRepository extends CrudRepository<PresenceCalendar, Long> {

//	PresenceCalendar getPresenceCalendar(Long id);
	/**
	 * Метод ищет книгу присутствий/календарь прошений по выбранной дате для показа
	 * внесенных в этот день в КП насельников
	 * 
	 * @param startDateWeek - дата заведения календаря
	 * @return итератор объектов класса для вывода в шаблон
	 */
	Iterable<PresenceCalendar> findByStartDateWeek(LocalDate startDateWeek);

	/**
	 * Смысл запроса в том, чтобы проверить, есть ли у насельника прошение на
	 * определенную дату (dateAbsent)
	 * 
	 * @param dateAbsent - дата отсутствия
	 * @param idResident - идентификатор насельника
	 * @return true, если на приходящую в запрос дату отсутствия (dateAbsent) есть
	 *         запись по данному насельнику (идентификация по айди)
	 */
//	Boolean findByDateAbsentAndIdResident(LocalDate dateAbsent, Long idResident);

	/**
	 * Находит прошение насельника в КП на день и выдает объект КП.
	 * 
	 * @param idResident
	 * @return
	 */
	Optional<PresenceCalendar> findByIdResidentAndDateAbsent(Long idResident, LocalDate dateAbsent);

	Optional<PresenceCalendar> findByWeekendsAndIdResident(boolean weekends, Long idResident);

	boolean findByDateAbsent(LocalDate dateAbsent);
}
