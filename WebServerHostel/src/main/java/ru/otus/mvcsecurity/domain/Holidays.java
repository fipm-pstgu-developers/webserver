package ru.otus.mvcsecurity.domain;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Holidays {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idHolidays;

	private Long idPeriod;
	private String nameHoliday;
	private LocalDate dateHoliday;

	@ManyToOne
	private Periods periods;

	public Holidays() {

	}

	public Holidays(Long idHolidays, Long idPeriod, String nameHoliday, LocalDate dateHoliday) {
		super();
		this.idHolidays = idHolidays;
		this.idPeriod = idPeriod;
		this.nameHoliday = nameHoliday;
		this.dateHoliday = dateHoliday;
	}

	public Long getIdHolidays() {
		return idHolidays;
	}

	public void setIdHolidays(Long idHolidays) {
		this.idHolidays = idHolidays;
	}


	public String getNameHoliday() {
		return nameHoliday;
	}

	public void setNameHoliday(String nameHoliday) {
		this.nameHoliday = nameHoliday;
	}

	public LocalDate getDateHoliday() {
		return dateHoliday;
	}

	public void setDateHoliday(LocalDate dateHoliday) {
		this.dateHoliday = dateHoliday;
	}

	public Long getIdPeriod() {
		return idPeriod;
	}

	public void setIdPeriod(Long idPeriod) {
		this.idPeriod = idPeriod;
	}

//	public Periods getPeriods() {
//		return periods;
//	}
//
//	public void setPeriods(Periods periods) {
//		this.periods = periods;
//	}

}
