package ru.otus.mvcsecurity.rest;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import ru.otus.mvcsecurity.domain.Periods;
import ru.otus.mvcsecurity.domain.Resident;
import ru.otus.mvcsecurity.domain.Violations;
import ru.otus.mvcsecurity.repostory.PeriodsRepository;
import ru.otus.mvcsecurity.repostory.ResidentRepository;
import ru.otus.mvcsecurity.repostory.ViolationsRepository;

@Controller
public class ViolationsController {
	@Autowired
	private ViolationsRepository repository;
	
	@Autowired
	private ResidentRepository resRepo;
	
	@Autowired
	private PeriodsRepository perRepo;

	@GetMapping("/violationsUpload")
	public String formViolationsUpload() {
		return "import_violations_file";
	}

	@PostMapping("/violationsUpload")
	public String fileViolationsUpload(@RequestParam("name") String name, @RequestParam("file") MultipartFile file) {

		/**
		 * Проверяем файл на пустоту. Если файл не пустой, то скачиваем его в папку
		 * проекта с кодом и берем в дальнейшем для парсинга. Внимание: дата парсится
		 * строго в последовательности 2007-10-31.
		 */
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				/**
				 * Скачиваем файл в папку с проектом
				 */
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(new File("src/main/resources/export/" + name /* + "-uploaded" */)));
				stream.write(bytes);
				stream.close();
//					System.out.println(/* + " в " + name + "-uploaded !" */);

				/**
				 * Достаем файл из папки с проектом
				 */
				File importFile = new File(
						"/home/feodor/LinuxProgramming/workspaceEclipse-Java/VKR/WebServer/src/main/resources/export/"
								+ name);
				// Read XSL file
				FileInputStream inputStream = new FileInputStream(importFile);

				// Get the workbook instance for XLS file
				HSSFWorkbook workbook = new HSSFWorkbook(inputStream);

				// Get first sheet from the workbook
				HSSFSheet sheet = workbook.getSheetAt(0);

				// Get iterator to all the rows in current sheet
				Iterator<Row> rowIterator = sheet.iterator();

//					int n;
//					System.out.println(repository);
//					Использовать поле delete только для тестового режима!!! 
//					repository.deleteAll();

				/**
				 * С помощью итератора проходимся по ячейкам строки и парсим каждую, записывая
				 * информацию в БД через поля объекта. Строковый тип данных ячейки в таблице
				 * имеют: ФИО, Факультет, Комментарий, период, пользователь, дата активности, начало периода, окончание периода. Целочисленный тип:
				 * курс.
				 */
				while (rowIterator.hasNext()) {
//						n = 0;
					Row row = rowIterator.next();
					System.out.println(row.getRowNum());

					Resident resident = new Resident();
					Violations violations = new Violations();
					Periods periods = new Periods();
					Boolean boo;

					if (row.getCell(0) == null)
						continue;
					/**
					 * Ищем насельников по ФИО, Курсу, Факультету: если есть несколько персон,
					 * работаем с каждой в цикле.
					 */
					Iterable<Resident> res = resRepo.findByFioContainsAndCourse(
							row.getCell(0).getStringCellValue(),
							(int) row.getCell(1).getNumericCellValue());
					for (Resident resident2 : res) {
						violations.setIdResident(resident2.getIdResident());
						violations.setCommentViolations(row.getCell(3).getStringCellValue());	 					
						/**
						 * Дата парсится только в виде 2022-05-08 !!!!!!!!
						 */
						violations.setDateViolations(LocalDate.parse(row.getCell(4).getStringCellValue())); 
						/**
						 * проверка на наличие одинаковых строк: если в БД есть уже строки с такими
						 * датами и названием, то возвращаем и сохраняем айди этих строк, если нет, то
						 * добавляем эту строку и полуаем айди строки
						 */
						boo = (perRepo.findByNamePeriodAndStartDatePeriodAndEndDatePeriod(
								row.getCell(5).getStringCellValue(),
								LocalDate.parse(row.getCell(6).getStringCellValue()),
								LocalDate.parse(row.getCell(7).getStringCellValue()))).isPresent();
						if (boo) {
							periods = perRepo.findByNamePeriodAndStartDatePeriodAndEndDatePeriod(
									row.getCell(5).getStringCellValue(),
									LocalDate.parse(row.getCell(6).getStringCellValue()),
									LocalDate.parse(row.getCell(7).getStringCellValue())).get();
							violations.setIdPeriod(periods.getIdPeriod());
						} else {
							periods.setNamePeriod(row.getCell(5).getStringCellValue());
							periods.setStartDatePeriod(LocalDate.parse(row.getCell(6).getStringCellValue()));
							periods.setEndDatePeriod(LocalDate.parse(row.getCell(7).getStringCellValue()));
							perRepo.save(periods);
						}

						violations.setIdPeriod(periods.getIdPeriod());
						

						// Необходимо сделать автоматическую фиксацию: кто внес положительную
						// активность, пока стоит заглушка и вносится вручную через поле в базе
						// TODO
						violations.setUserSystem(row.getCell(8).getStringCellValue()); 
						violations.setPointsNumber((int) row.getCell(9).getNumericCellValue()); 
						if((int) row.getCell(10).getNumericCellValue() == 1) {
							violations.setExplanatoryRespectful("Уважительная объяснительная");
						}
						else {
							violations.setNotExplanatoryRespectful("Неуважительная объяснительная");
						}
						
						
						repository.save(violations);
					}

				}

				return "import_violations_file";

			} catch (Exception e) {
				System.out.println("Вам не удалось загрузить " + name + " => " + e.getMessage());
				return "import_violations_file";
			}
		} else {
			System.out.println("Вам не удалось загрузить " + name + " потому что файл пустой.");
			return "import_violations_file";
		}
	}

}
