/**
 * 
 */
package ru.otus.mvcsecurity.repostory;

import org.springframework.data.repository.CrudRepository;

import ru.otus.mvcsecurity.domain.CleaningRooms;

/**
 * @author Fedor Karpov
 *
 */
public interface CleaningRoomsRepository extends CrudRepository<CleaningRooms, Long> {

}
