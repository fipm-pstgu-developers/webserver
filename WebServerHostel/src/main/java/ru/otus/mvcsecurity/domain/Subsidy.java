package ru.otus.mvcsecurity.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Класс "Субсидии" необходим для фиксации выделенных дотаций насельникам. Нужен
 * для отображения на личной страничке насельника и отчетов.
 *
 * @author Fedor Karpov
 * @version 1.0
 * @param idSubsidy      - идентификатор субсидии
 * @param idResident     - идентификатор насельника, получающего субсидию
 * @param idPeriods      - идентификатор периода
 * @param commentSubsidy - комментарий к выданной субсидии (за что выдалась
 *                       субсидия, какие заслуги)
 * @param user           - пользователь, вписавший в БД информацию по субсидии
 *                       насельника
 * @param summSubsidy    - сумма выданной субсидии
 * @param dateAssignment - дата назначения субсидии
 */

@Entity
public class Subsidy {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idSubsidy;

	private Long idResident;
	private Long idPeriods;
	private String commentSubsidy;
	@Column(name = "user_b")
	private String user;
	private Integer summSubsidy;
	private LocalDate dateAssignment;

	public Subsidy() {

	}

	public Subsidy(Long idSubsidy, Long idResident, String commentSubsidy, String user, Integer summSubsidy,
			LocalDate dateAssignment, Long idPeriods) {
		super();
		this.idPeriods = idPeriods;
		this.idSubsidy = idSubsidy;
		this.idResident = idResident;
		this.commentSubsidy = commentSubsidy;
		this.user = user;
		this.summSubsidy = summSubsidy;
		this.dateAssignment = dateAssignment;
	}

	public Long getIdSubsidy() {
		return idSubsidy;
	}

	public void setIdSubsidy(Long idSubsidy) {
		this.idSubsidy = idSubsidy;
	}

	public Long getIdResident() {
		return idResident;
	}

	public void setIdResident(Long idResident) {
		this.idResident = idResident;
	}

	public String getCommentSubsidy() {
		return commentSubsidy;
	}

	public void setCommentSubsidy(String commentSubsidy) {
		this.commentSubsidy = commentSubsidy;
	}

	public Long getIdPeriods() {
		return idPeriods;
	}

	public void setIdPeriods(Long idPeriods) {
		this.idPeriods = idPeriods;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Integer getSummSubsidy() {
		return summSubsidy;
	}

	public void setSummSubsidy(Integer summSubsidy) {
		this.summSubsidy = summSubsidy;
	}

	public LocalDate getDateAssignment() {
		return dateAssignment;
	}

	public void setDateAssignment(LocalDate dateAssignment) {
		this.dateAssignment = dateAssignment;
	}

}
