package ru.otus.mvcsecurity.repostory;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ru.otus.mvcsecurity.domain.Resident;

/**
 * Репозиторий регламентирует методы запросов к объектам классов(соответственно
 * и сущностям) в БД.
 * 
 * @author Fedor Karpov
 */

@Repository
public interface ResidentRepository extends CrudRepository<Resident, Long> {

	/**
	 * Запросы к сущности можно строить прямо из имени метода. Для этого
	 * используется механизм префиксов find…By, read…By, query…By, count…By, и
	 * get…By, далее от префикса метода начинает разбор остальной части. Вводное
	 * предложение может содержать дополнительные выражения, например, Distinct.
	 * Далее первый By действует как разделитель, чтобы указать начало фактических
	 * критериев. Можно определить условия для свойств сущностей и объединить их с
	 * помощью And и Or.
	 */

	/**
	 * Поиск насельника по фио (с помощью подстроки - containes).
	 * 
	 * @param fio - ФИО
	 * @return - информацию по насельнику, в виде итератора объектов класса
	 */
	Iterable<Resident> findByFioContains(String fio);

	/**
	 * Поиск насельника по фио, курсу, факультету (с помощью подстроки - containes).
	 * 
	 * @param fio     - ФИО
	 * @param faculty - факультет
	 * @param course  - курс
	 * @return информацию по насельнику в виде итератора объектов класса
	 */
	Iterable<Resident> findByFioContainsAndCourse(String fio, Integer course);

	Optional<Resident> findByFioAndCourseAndFaculty(String fio, Integer course, String faculty);

//	List<Resident> allResident();
	Optional<Resident> getByRoomNumber(Integer roomNumber);

}
