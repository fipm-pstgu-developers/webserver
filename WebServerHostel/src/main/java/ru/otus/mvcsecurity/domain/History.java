package ru.otus.mvcsecurity.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Класс "История" используется для фиксации переселений насельника из комнаты в
 * комнату. История состоит из событий (заселен в комнату, выселен и тд).
 * 
 * @author Fedor Karpov
 * @version 1.0
 * @param idHistory    - идентификатор истории
 * @param idResident   - идентификатор насельника
 * @param dateHistory  - дата произошедшего события в истории
 * @param eventHistory - произшедшее событие (заселен в комнату и тд)
 * @param user         - пользователь, сделавший запись в истории
 */

@Entity
public class History {	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idHistory;

	private Long idResident;
	private LocalDate dateHistory;
	private String eventHistory;
	@Column(name = "user_b")
	private String user;

	public History() {

	}

	public History(Long idHistory, Long idResident, LocalDate dateHistory, String eventHistory, String user) {
		super();
		this.idHistory = idHistory;
		this.idResident = idResident;
		this.dateHistory = dateHistory;
		this.eventHistory = eventHistory;
		this.user = user;
	}

	public Long getIdHistory() {
		return idHistory;
	}

	public void setIdHistory(Long idHistory) {
		this.idHistory = idHistory;
	}

	public Long getIdResident() {
		return idResident;
	}

	public void setIdResident(Long idResident) {
		this.idResident = idResident;
	}

	public LocalDate getDateHistory() {
		return dateHistory;
	}

	public void setDateHistory(LocalDate dateHistory) {
		this.dateHistory = dateHistory;
	}

	public String getEventHistory() {
		return eventHistory;
	}

	public void setEventHistory(String eventHistory) {
		this.eventHistory = eventHistory;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

}
