package ru.otus.mvcsecurity.rest;

import java.io.File;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import org.springframework.web.multipart.MultipartFile;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
//import org.o7planning.apachepoiexcel.model.Employee;
//import org.o7planning.apachepoiexcel.model.EmployeeDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ru.otus.mvcsecurity.domain.History;
import ru.otus.mvcsecurity.domain.Resident;
import ru.otus.mvcsecurity.domain.Rooms;
import ru.otus.mvcsecurity.repostory.HistoryRepository;
import ru.otus.mvcsecurity.repostory.ResidentRepository;
import ru.otus.mvcsecurity.repostory.RoomsRepository;

/**
 * Контроллер, отвечающий за информацию по насельнику в БД и браузере
 * 
 * @author Fedor Karpov
 */

@Controller
public class ResidentController {

	@Autowired
	private ResidentRepository repository;

	@Autowired
	private RoomsRepository roomsRepo;

	@Autowired
	private HistoryRepository hisRepo;

	@Autowired
	public void ResidentController(ResidentRepository repository) {
		this.repository = repository;
	}

	/**
	 * Метод addResident выполняет добавление насельника в БД через форму person.
	 * Метод form необходим для выполнения Пост запроса addResident.
	 * 
	 * @autor Fedor Karpov
	 * @version 1.0
	 * @param fio               - ФИО
	 * @param course            - курс
	 * @param faculty           - факультет
	 * @param learningCondition - условия обучения
	 * @param direction         - направление
	 * @param group             - группа
	 * @param educationLevel    - уровень образования
	 * @param citizenShip       - гражданство
	 * @param dateOfBirth       - дата рождения
	 * @param monthOfBirth      - месяц рождения
	 * @param yearOfBirth       - год рождения
	 * @param gender            - пол. Параметр true отвечает за обозначение
	 *                          мужского пола. Важно писать четко: "Мужской" в
	 *                          строке форме
	 * @param telephonNumber    - телефон
	 * @param address           -
	 * @param email             -
	 * @param pointSemestr      - баллы
	 * @param roomNumber        - номер комнаты
	 * @param model             - используется для связи формы person и метода
	 *                          addResident
	 * @return person - возвращает форму отрисовки анкеты для браузера
	 */
	@GetMapping("/input")
	public String form() {
		return "person";
	}

	@PostMapping("/input")
	public String addResident(@RequestParam String fio, @RequestParam(required = false) Integer course,

			@RequestParam String faculty,

			@RequestParam String learningCondition, @RequestParam String direction, @RequestParam String group,

			@RequestParam String educationLevel, @RequestParam String citizenShip, @RequestParam String dateOfBitrh,

			@RequestParam Integer monthOfBirth, @RequestParam Integer yearOfBirth, @RequestParam String gender,

			@RequestParam String telephonNumber, @RequestParam Integer pointSemestr, @RequestParam Integer roomNumber,

			@RequestParam String address, @RequestParam String email, Model model) {
		Resident resident = new Resident();
		Rooms room = new Rooms();
		History history = new History();
		Boolean flag = false;
		resident.setFio(fio);
		System.out.println("course: " + course);
		resident.setCourse(course);
		resident.setFaculty(faculty);
		resident.setLearningCondition(learningCondition);
		resident.setDirection(direction);
		resident.setGroup(group);
		resident.setEducationLevel(educationLevel);
		resident.setCitizenShip(citizenShip);

		System.out.println("dateOfBitrh: " + dateOfBitrh);
		resident.setLocalDateOfBitrh(/* (LocalDate) */ LocalDate.parse(dateOfBitrh));
		resident.setMonthOfBirth(monthOfBirth);
		resident.setYearOfBirth(yearOfBirth);

		if (gender == "Мужской")
			resident.setGender(true);
		else
			resident.setGender(false);

		resident.setTelephonNumber(telephonNumber);
		resident.setPointSemestr(pointSemestr);
		//
		if ((roomsRepo.findByNumberRoom(roomNumber)).isPresent()) {
			resident.setRoomNumber(roomNumber);
			room = (roomsRepo.findByNumberRoom(roomNumber)).get();
			if (gender == "Мужской") {
				room.setAttachment(true);
			} else
				room.setAttachment(true);
			roomsRepo.save(room);

		} else {
			resident.setRoomNumber(roomNumber);
			room.setNumberRoom(roomNumber);

			if (gender == "Мужской") {
				room.setAttachment(true);
			} else
				room.setAttachment(true);
			roomsRepo.save(room);
		}

		resident.setAddress(address);
		resident.setEmail(email);

		repository.save(resident);

		model.addAttribute("r", resident);

		return "person";
	}

	/**
	 * Метод, показывающий личную страничку насельника по айди, переданному в пути
	 * запроса. Так как используется для выдачи по айди, которое передается в виде
	 * текста из метода getResidentByFio, то конвертируем айди в лонг тип для БД.
	 * 
	 * @param id    - метод получает id насельника, по которому надо показать личную
	 *              информацию.
	 * @param model - объект связывает контроллер и шаблон отрисовки в браузере
	 * @return person_view - шаблон для отрисовки в браузере личной странички
	 *         студента
	 */
	@GetMapping("/residents/{id}") // Показывают страницу насельника со всеми данными по нему.
	public String getResident(@PathVariable String id, Model model) {
		Optional<Resident> r = repository.findById(Long.parseLong(id));
		System.out.println(r.get());
		model.addAttribute("r", r.get());

		return "person_view";

	}

	@GetMapping("/test")
	public String testRedirect() {
		return "redirect:/resident";
	}

	/**
	 * Ищет по фио и показывает всех насельников с похожими названиями (поиск по
	 * подстроке).
	 * 
	 * @param fio   - форма передает фио насельника для поиска
	 * @param model - модель связывания шаблона и
	 * @return person_view - шаблон для отрисовки страницы насельника
	 */
	@GetMapping("/resident")
	public String getResidentForm() {

		return "main";
	}

	@PostMapping("/resident")
	public String getResidentByFio(@RequestParam String fio, Model model) {
		Iterable<Resident> resident = repository.findByFioContains(fio);
		for (Resident resident2 : resident) {
			System.out.println("resident2 fio: " + resident2.getFio());
		}

		model.addAttribute("resident", resident);
		return "table_persons";
	}

	/**
	 * Метод необходим для показа всех насельников БД.
	 * 
	 * @param model - модель для связывания контроллера и шаблона
	 * @return table_persons - шаблон для отрисовки всех насельников БД
	 */
	@GetMapping("/residents/all")
	public String getAllResident(Model model) {
		List<Resident> r = (List<Resident>) repository.findAll();

		model.addAttribute("resident", r);
		return "table_persons";
	}

	@GetMapping("/upload")
	public String formUpload() {
		return "import_file";
	}

	/**
	 * Метод используется для импорта файла c личными данными насельника и его
	 * парсинга в БД
	 * 
	 * @param name - имя импортируемого файла
	 * @param file - файл, который импортируется
	 * @return import_file - возвращает шаблон, в котором размечена форма для
	 *         импорта файла
	 */
	@PostMapping("/upload")
	public String handleFileUpload(@RequestParam("name") String name, @RequestParam("file") MultipartFile file) {

		/**
		 * Проверяем файл на пустоту. Если файл не пустой, то скачиваем его в папку
		 * проекта с кодом и берем в дальнейшем для парсинга. Внимание: дата парсится
		 * строго в последовательности 2007-10-31. Номер телефона парсится в виде
		 * +7(968)015-70-41.
		 */
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				/**
				 * Скачиваем файл в папку с проектом
				 */
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(new File("src/main/resources/export/" + name /* + "-uploaded" */)));
				stream.write(bytes);
				stream.close();
				/**
				 * Достаем файл из папки с проектом
				 */
				File importFile = new File(
						"/home/feodor/LinuxProgramming/workspaceEclipse-Java/VKR/WebServerHostel/src/main/resources/export/"
								+ name);
				// Read XSL file
				FileInputStream inputStream = new FileInputStream(importFile);

				// Get the workbook instance for XLS file
				HSSFWorkbook workbook = new HSSFWorkbook(inputStream);

				// Get first sheet from the workbook
				HSSFSheet sheet = workbook.getSheetAt(0);

				// Get iterator to all the rows in current sheet
				Iterator<Row> rowIterator = sheet.iterator();

				// ДЛЯ ТЕСТИРОВАНИЯ ФУНКЦИИ УДАЛЯЛИСЬ ЗНАЧЕНИЯ - в рабочем прототипе это не
				// нужно
				// int n;
				// System.out.println(repository);
				// repository.deleteAll();

				/**
				 * С помощью итератора проходимся по ячейкам строки и парсим каждую, записывая
				 * информацию в БД через поля объекта
				 */
				while (rowIterator.hasNext()) {
					Resident resident = new Resident();
					Rooms room = new Rooms();
					int roomNumber;
					boolean gender;
//					n = 0;
					Row row = rowIterator.next();
					System.out.println(row.getRowNum());

					if (row.getCell(0) == null)
						continue;

					resident.setFio(row.getCell(0).getStringCellValue());
					resident.setCourse((int) row.getCell(1).getNumericCellValue());
					resident.setFaculty(row.getCell(2).getStringCellValue());
					resident.setLearningCondition(row.getCell(3).getStringCellValue());
					resident.setDirection(row.getCell(4).getStringCellValue());
					System.out.println(row.getCell(5).getCellType());
					resident.setGroup(row.getCell(5).getStringCellValue());

					resident.setEducationLevel(row.getCell(6).getStringCellValue());
					resident.setCitizenShip(row.getCell(7).getStringCellValue());
					System.out.println(row.getCell(7).getCellType());
					System.out.println(LocalDate.parse(row.getCell(8).getStringCellValue()));
					resident.setLocalDateOfBitrh(LocalDate.parse(row.getCell(8).getStringCellValue()));
					resident.setMonthOfBirth((int) row.getCell(9).getNumericCellValue());
					resident.setYearOfBirth((int) row.getCell(10).getNumericCellValue());
					gender = row.getCell(11).getStringCellValue().contentEquals("Мужской");
					if (gender == true) {
						resident.setGender(true);
					} else {
						resident.setGender(false);
					}
					resident.setTelephonNumber(row.getCell(12).getStringCellValue());
					resident.setAddress(row.getCell(13).getStringCellValue());
					resident.setEmail(row.getCell(14).getStringCellValue());
					resident.setPointSemestr((int) row.getCell(15).getNumericCellValue());
					roomNumber = (int) row.getCell(16).getNumericCellValue();
					if ((roomsRepo.findByNumberRoom(roomNumber)).isPresent()) {
						resident.setRoomNumber(roomNumber);
						room = (roomsRepo.findByNumberRoom(roomNumber)).get();
						if (gender == true) {
							room.setAttachment(true);
						} else
							room.setAttachment(true);
						roomsRepo.save(room);

					} else {
						resident.setRoomNumber(roomNumber);
						room.setNumberRoom(roomNumber);
						room.setContentRoom(2);

						if (gender == true) {
							room.setAttachment(true);
						} else
							room.setAttachment(false);

						roomsRepo.save(room);
					}

					repository.save(resident);

				}
				return "import_file";

			} catch (Exception e) {
				System.out.println("Вам не удалось загрузить " + name + " => " + e.getMessage());
				return "import_file";
			}
		} else {
			System.out.println("Вам не удалось загрузить " + name + " потому что файл пустой.");
			return "import_file";
		}
	}
	// Результатом работы метода является занесение в БД информации о заселении
	// студента в комнату и присвоения комнате и насельнику айдишников
//	TODO
//	public creatingRoomResident{
//		Принимает айди насельника и номер комнаты
//		По номеру комнаты ищется ее айди в БД в таблице комнаты
//		Айди комнаты заносится в таблицу История, как и другие необходимые айдишники
//		Теперь у нас есть ведение Истории комнат 

//	}
}

//@GetMapping
//public <FileInputStream> String importFileOffice(Model model) {
//	
//	// получаем файл в формате xlsx
//	FileInputStream file = new FileInputStream(new File());
//	             
//	// получаем экземпляр XSSFWorkbook для обработки xlsx файла 
//	XSSFWorkbook workbook = new XSSFWorkbook (file);
//	 
//	// выбираем первый лист для обработки 
//	// нумерация начинается из 0
//	XSSFSheet sheet = workbook.getSheetAt(0);
//	 
//	// получаем Iterator по всем строкам в листе
//	Iterator<Row> rowIterator = sheet.iterator();
//	 
//	// получаем Iterator по всем ячейкам в строке
//	Iterator<Cell> cellIterator = row.cellIterator();
//
//	
//	return "importFileOffice";
//}
