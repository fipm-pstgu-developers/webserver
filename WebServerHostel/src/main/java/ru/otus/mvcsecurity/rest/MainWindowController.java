package ru.otus.mvcsecurity.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class MainWindowController {
	
	@GetMapping("/mainMenu")
	public String formMenu() {
		return "main";
	} 	
	
	@PostMapping("/mainMenu")
	public String menu() {
		
		return "main";
	}
	
	
}
