/**
 * 
 */
package ru.otus.mvcsecurity.repostory;

import org.springframework.data.repository.CrudRepository;

import ru.otus.mvcsecurity.domain.PositiveActivity;

/**
 * @author feodor
 *
 */
public interface PositiveActivityRepository extends CrudRepository<PositiveActivity, Long> {

}
