/**
 * 
 */
package ru.otus.mvcsecurity.repostory;

import org.springframework.data.repository.CrudRepository;

import ru.otus.mvcsecurity.domain.History;

/**
 * @author feodor
 *
 */
public interface HistoryRepository extends CrudRepository<History, Long> {

}
