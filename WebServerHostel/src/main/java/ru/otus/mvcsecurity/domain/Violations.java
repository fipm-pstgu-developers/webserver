package ru.otus.mvcsecurity.domain;

import java.time.LocalDate;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Класс "Нарушения", описывающий нарушения студента: опоздания, пропуски в
 * общежитии, скоромная еда в пост и т.д. Нужен для формирования отчетов и
 * личной странички студента.
 * 
 * @author Fedor Karpov
 * @version 3.0
 * @param notExplanatoryRespectful - НЕ уважительная объяснительная. Необходима
 *                                 для личной страницы студента.
 * @param explanatoryRespectful    - уважительная объяснительная. Необходима для
 *                                 личной страницы студента.
 * @param pointsNumber             - количество баллов
 * @param idViolations             - идентификатор нарушения
 * @param idPeriod                 - идентификатор периода
 * @param idResident               - идентификатор насельника
 * @param dateViolations           - дата нарушения
 * @param commentViolations        - комментарий к нарушению (в чем заключается
 *                                 само нарушение)
 * @param userSystem               - пользователь системы, который ввел
 *                                 нарушение
 */
@Entity
public class Violations {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idViolations;

	private Long idResident;
	private Long idPeriod;
	private Integer pointsNumber;
	private LocalDate dateViolations;
	private String commentViolations;
	private String explanatoryRespectful;
	private String notExplanatoryRespectful;
	private String userSystem;

	@ManyToOne
	private Periods periods;

	
	public Violations() {

	}

	public Violations(Long idViolations, Long idResident, Long idPeriod, Integer pointsNumber, LocalDate dateViolations,
			String commentViolations, String explanatoryRespectful, String notExplanatoryRespectful) {
		super();
		this.idViolations = idViolations;
		this.idResident = idResident;
		this.idPeriod = idPeriod;
		this.pointsNumber = pointsNumber;
		this.dateViolations = dateViolations;
		this.commentViolations = commentViolations;
		this.explanatoryRespectful = explanatoryRespectful;
		this.notExplanatoryRespectful = notExplanatoryRespectful;
	}

	public Long getIdViolations() {
		return idViolations;
	}

	public void setIdViolations(Long idViolations) {
		this.idViolations = idViolations;
	}

	public Long getIdResident() {
		return idResident;
	}

	public void setIdResident(Long idResident) {
		this.idResident = idResident;
	}

	public Long getIdPeriod() {
		return idPeriod;
	}

	public void setIdPeriod(Long idPeriod) {
		this.idPeriod = idPeriod;
	}

	public Integer getPointsNumber() {
		return pointsNumber;
	}

	public void setPointsNumber(Integer pointsNumber) {
		this.pointsNumber = pointsNumber;
	}

	public LocalDate getDateViolations() {
		return dateViolations;
	}

	public void setDateViolations(LocalDate dateViolations) {
		this.dateViolations = dateViolations;
	}

	public String getCommentViolations() {
		return commentViolations;
	}

	public void setCommentViolations(String commentViolations) {
		this.commentViolations = commentViolations;
	}

	public String getExplanatoryRespectful() {
		return explanatoryRespectful;
	}

	public void setExplanatoryRespectful(String explanatoryRespectful) {
		this.explanatoryRespectful = explanatoryRespectful;
	}

	public String getNotExplanatoryRespectful() {
		return notExplanatoryRespectful;
	}

	public void setNotExplanatoryRespectful(String notExplanatoryRespectful) {
		this.notExplanatoryRespectful = notExplanatoryRespectful;
	}

	public String getUserSystem() {
		return userSystem;
	}
	
	public void setUserSystem(String userSystem) {
		this.userSystem = userSystem;
	}
}
