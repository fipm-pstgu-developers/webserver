package ru.otus.mvcsecurity.domain;

import java.time.LocalDate;

import java.util.Set;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Класс "Насельник" с личными данными. Необходим для составления личной
 * страницы насельника.
 * 
 * @autor Fedor Karpov
 * @version 1.0
 * @param idResident        - идентификатор насельника
 * @param fio               - ФИО
 * @param course            - курс
 * @param faculty           - факультет
 * @param learningCondition - условия обучения
 * @param direction         - направление
 * @param group             - группа
 * @param educationLevel    - уровень образования
 * @param citizenShip       - гражданство
 * @param dateOfBirth       - дата рождения
 * @param monthOfBirth      - месяц рождения
 * @param yearOfBirth       - год рождения
 * @param gender            - пол
 * @param telephonNumber    - телефон
 * @param address           -
 * @param email             -
 * @param pointSemestr      - баллы
 * @param roomNumber        - номер комнаты
 */

@Entity
public class Resident {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idResident;

	private String fio;
	private Integer course;
	private String faculty;
	private String learningCondition;
	private String direction;
	@Column(name = "group_b")
	private String group;
	private String educationLevel;
	private String citizenShip;
	private LocalDate dateOfBitrh;
	private Integer monthOfBirth;
	private Integer yearOfBirth;
	private boolean gender;
	private String telephonNumber;
	private Integer pointSemestr;
	private Integer roomNumber;
	private String address;
	private String email;

	@OneToMany(mappedBy = "resident")
	private Set<PresenceCalendar> presenceCalendar;

	public Resident() {
	}

	public Resident(Long idResident, Integer course, String faculty, String learningCondition, String direction,
			String group, String educationLevel, String citizenShip, LocalDate dateOfBitrh, Integer monthOfBirth,
			Integer yearOfBirth, boolean gender, String telephonNumber, Integer pointsPerSemestr, String fio,
			Integer roomNumber, String address, String email) {
		super();
		this.idResident = idResident;
		this.course = course;
		this.faculty = faculty;
		this.learningCondition = learningCondition;
		this.direction = direction;
		this.group = group;
		this.educationLevel = educationLevel;
		this.citizenShip = citizenShip;
		this.dateOfBitrh = dateOfBitrh;
		this.monthOfBirth = monthOfBirth;
		this.yearOfBirth = yearOfBirth;
		this.gender = gender;
		this.telephonNumber = telephonNumber;
		this.pointSemestr = pointsPerSemestr;
		this.fio = fio;
		this.roomNumber = roomNumber;
		this.address = address;
		this.email = email;
	}

	public Integer getPointSemestr() {
		return pointSemestr;
	}

	public void setPointSemestr(Integer pointSemestr) {
		this.pointSemestr = pointSemestr;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getIdResident() {
		return idResident;
	}

	public void setIdResident(Long idResident) {
		this.idResident = idResident;
	}

	public Integer getCourse() {
		return course;
	}

	public void setCourse(Integer course) {
		this.course = course;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	public String getLearningCondition() {
		return learningCondition;
	}

	public void setLearningCondition(String learningCondition) {
		this.learningCondition = learningCondition;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getEducationLevel() {
		return educationLevel;
	}

	public void setEducationLevel(String educationLevel) {
		this.educationLevel = educationLevel;
	}

	public String getCitizenShip() {
		return citizenShip;
	}

	public void setCitizenShip(String citizenShip) {
		this.citizenShip = citizenShip;
	}

	public LocalDate getDateOfBitrh() {
		return dateOfBitrh;
	}

	public void setLocalDateOfBitrh(LocalDate dateOfBitrh) {
		this.dateOfBitrh = dateOfBitrh;
	}
//	public void setDateOfBitrh(Date dateOfBitrh) {
//		this.dateOfBitrh = /*(LocalDate)*/ dateOfBitrh;
//	}

	public Integer getMonthOfBirth() {
		return monthOfBirth;
	}

	public void setMonthOfBirth(Integer monthOfBirth) {
		this.monthOfBirth = monthOfBirth;
	}

	public Integer getYearOfBirth() {
		return yearOfBirth;
	}

	public void setYearOfBirth(Integer yearOfBirth) {
		this.yearOfBirth = yearOfBirth;
	}

	public boolean isGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public String getTelephonNumber() {
		return telephonNumber;
	}

	public void setTelephonNumber(String telephonNumber) {
		this.telephonNumber = telephonNumber;
	}

	public Integer getPointsPerSemestr() {
		return pointSemestr;
	}

	public void setPointsPerSemestr(Integer pointsPerSemestr) {
		this.pointSemestr = pointsPerSemestr;
	}

	public String getFio() {
		return fio;
	}

	public void setFio(String fio) {
		this.fio = fio;
	}

	public Integer getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(Integer roomNumber) {
		this.roomNumber = roomNumber;
	}

}
