package ru.otus.mvcsecurity.rest;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ru.otus.mvcsecurity.domain.Periods;
import ru.otus.mvcsecurity.repostory.PeriodsRepository;

@Controller
public class PeriodsController {

	@Autowired
	PeriodsRepository repository;

	public void getPer() {
		Periods periods1 = new Periods();
		periods1.setNamePeriod("Первый семестр 21/22");
		periods1.setStartDatePeriod(LocalDate.parse("2022-08-29"));
		periods1.setEndDatePeriod(LocalDate.parse("2022-12-31"));
		repository.save(periods1);

	}

	/**
	 * Как проверить, попадает ли дата в промежуток дат хранящихся в БД? 1) Вызвать
	 * все объекты таблицы периодов 2) Через итерацию у каждого объекта взять начало
	 * и конец периода 3) Передать начало и конец периода в метод, считающий разницу
	 * дней этих периодов 4) Получить массив из разницы этих дат и сравнивая с
	 * каждой датой массива дату, которую передаем в самом начале, чтобы убедиться,
	 * попадает она в промежуток или нет. 5) Если на каком - то этапе итерации
	 * сравнение выдает true, то выходим из циклов и по промежуткам, которые ввели
	 * для измерения разницы, определяем айди периода 6) после определения айди,
	 * передаем этот айди в имени нашего метода, чтобы внести этот айди в
	 * требующуюся таблицу. 7) Если сравнение на всех датах выдает false, то
	 * переходим к следующей итерации объектов таблицы периодов и смотрим в них 8)
	 * Если дата не находится, то warning
	 * 
	 * @param dateAbsent
	 */
	public Periods gettingIntoPeriod(LocalDate dateAbsent) {

		Long id = null;
		Periods per = new Periods();

		/**
		 * 1) Вызвать все объекты таблицы периодов
		 */
//		List<Periods> perList = (List<Periods>) repository.findAllPeriods();

		/**
		 * 2) Через итерацию у каждого объекта взять начало и конец периода
		 */
		for (Periods periods : repository.findAll()) {

			/**
			 * 3) Передать начало и конец периода в метод, считающий разницу дней этих
			 * периодов
			 * 
			 */
			List<LocalDate> daysBetween = getDatesBetween(periods.getStartDatePeriod(), periods.getEndDatePeriod());

			/**
			 * 5) Если на каком - то этапе итерации сравнение выдает true, то выходим из
			 * циклов и по промежуткам, которые ввели для измерения разницы, определяем айди
			 * периода 6) после определения айди, передаем этот айди в имени нашего метода,
			 * чтобы внести этот айди в требующуюся таблицу.
			 */
			if (daysBetween.contains(dateAbsent)) {
				per = periods;
				break;
			}

		}
//		TODO - нужно ли возвращать periods таким образом?
		return per;
	}

	public static List<LocalDate> getDatesBetween(LocalDate startDate, LocalDate endDate) {
		long numOfDaysBetween = ChronoUnit.DAYS.between(startDate, endDate.plusDays(1));
		return IntStream.iterate(0, i -> i + 1).limit(numOfDaysBetween).mapToObj(i -> startDate.plusDays(i))
				.collect(Collectors.toList());
	}

}
