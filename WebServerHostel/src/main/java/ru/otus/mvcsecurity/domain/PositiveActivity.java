package ru.otus.mvcsecurity.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Класс "Положительной активности" насельника, характеризует положительную
 * деятельность проживающего.
 * 
 * @author Fedor Karpov
 * @version 1.0
 * @param idActivity         - идентификатор активности
 * @param idResident         - идентификатор насельника
 * @param commentPositiveAct - комментарий к положительной активности(в чем
 *                           заключается положительная активность)
 * @param user               - пользователь в системе, оставивший комментарий по
 *                           поводу деятельности насельника
 * @param datePositiveAct    - дата положительной активности
 * @param idPeriod           - идентификатор периода, в которой совершилась
 *                           активность
 */

@Entity
public class PositiveActivity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idActivity;

	private Long idResident;
	private String commentPositiveAct;
	@Column(name = "user_b")
	private String user;
	private LocalDate datePositiveAct;
	private Long idPeriod;

	@ManyToOne
	private Periods periods;

	public PositiveActivity() {

	}

	public PositiveActivity(Long idActivity, Long idResident, String commentPositiveAct, String user,
			LocalDate datePositiveAct, Long idPeriod) {
		super();
		this.idActivity = idActivity;
		this.idResident = idResident;
		this.commentPositiveAct = commentPositiveAct;
		this.user = user;
		this.datePositiveAct = datePositiveAct;
		this.idPeriod = idPeriod;
	}

	public Long getIdActivity() {
		return idActivity;
	}

	public void setIdActivity(Long idActivity) {
		this.idActivity = idActivity;
	}

	public Long getIdResident() {
		return idResident;
	}

	public void setIdResident(Long idResident) {
		this.idResident = idResident;
	}

	public String getCommentPositiveAct() {
		return commentPositiveAct;
	}

	public void setCommentPositiveAct(String commentPositiveAct) {
		this.commentPositiveAct = commentPositiveAct;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public LocalDate getDatePositiveAct() {
		return datePositiveAct;
	}

	public void setDatePositiveAct(LocalDate datePositiveAct) {
		this.datePositiveAct = datePositiveAct;
	}

	public Long getIdPeriod() {
		return idPeriod;
	}

	public void setIdPeriod(Long idPeriod) {
		this.idPeriod = idPeriod;
	}

}
