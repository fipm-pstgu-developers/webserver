package ru.otus.mvcsecurity.repostory;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import ru.otus.mvcsecurity.domain.Holidays;

public interface HolidaysRepository extends CrudRepository<Holidays, Long> {
	
	Optional<Holidays> findByDateHoliday(LocalDate date); 

}
