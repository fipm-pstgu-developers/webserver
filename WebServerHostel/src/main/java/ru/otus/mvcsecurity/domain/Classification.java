package ru.otus.mvcsecurity.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Класс "Классификатор" нужен для разделения отрицательной активности по видам
 * (курение, мат и тд).
 * 
 * @author Fedor Karpov
 * @version 1.0
 * @param idClassification   - идентификатор классификатора
 * @param nameClassification - название идентификатора
 * 
 */

@Entity
public class Classification {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idClassification;

	private String nameClassification;

	public Classification() {

	}

	public Classification(Long idClassification, String nameClassification) {
		super();
		this.idClassification = idClassification;
		this.nameClassification = nameClassification;
	}

	public Long getIdClassification() {
		return idClassification;
	}

	public void setIdClassification(Long idClassification) {
		this.idClassification = idClassification;
	}

	public String getNameClassification() {
		return nameClassification;
	}

	public void setNameClassification(String nameClassification) {
		this.nameClassification = nameClassification;
	}

}
