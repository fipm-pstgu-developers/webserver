package ru.otus.mvcsecurity.domain;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Класс "Таблица периодов" необходим для разделения положительной и
 * отрицательной активности по семестрам на личной страничке студента и отчетов.
 * 
 * @author Fedor Karpov
 * @version 1.0
 * @param idPeriod        - идентификатор пероида
 * @param namePeriod      - название периода (1 семетр 21/22 и т.д.)
 * @param startDatePeriod - начало периода (01.01 - 31.08 пр)
 * @param endDatePeriod   - конец периода
 */

@Entity
public class Periods {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idPeriod;

	private String namePeriod;
	private LocalDate startDatePeriod;
	private LocalDate endDatePeriod;
	
	@OneToMany(mappedBy ="periods")
    private Set<PositiveActivity> positiveActivity;
	
	@OneToMany(mappedBy ="periods")
    private Set<Violations> voilations;
	
	@OneToMany(mappedBy ="periods")
    private Set<Holidays> holidays;
	
	@OneToMany(mappedBy ="periods")
    private Set<NegativeActivity> negativeActivity;
	
	public Periods() {
	}

	public Periods(Long idPeriod, String namePeriod, LocalDate startDatePeriod, LocalDate endDatePeriod) {
		super();
		this.idPeriod = idPeriod;
		this.namePeriod = namePeriod;
		this.startDatePeriod = startDatePeriod;
		this.endDatePeriod = endDatePeriod;
	}

	public Long getIdPeriod() {
		return idPeriod;
	}

	public void setIdPeriod(Long idPeriod) {
		this.idPeriod = idPeriod;
	}

	public String getNamePeriod() {
		return namePeriod;
	}

	public void setNamePeriod(String namePeriod) {
		this.namePeriod = namePeriod;
	}

	public LocalDate getStartDatePeriod() {
		return startDatePeriod;
	}

	public void setStartDatePeriod(LocalDate startDatePeriod) {
		this.startDatePeriod = startDatePeriod;
	}

	public LocalDate getEndDatePeriod() {
		return endDatePeriod;
	}

	public void setEndDatePeriod(LocalDate endDatePeriod) {
		this.endDatePeriod = endDatePeriod;
	}

}
