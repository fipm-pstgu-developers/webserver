package ru.otus.mvcsecurity.domain;

import java.time.LocalDate;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Класс "Отрицательная активность", описывающий отрицательную активность
 * студента: нахамил администрации, ругань, мат, курение и прочее - тяжкие
 * проступки. Нужен для формирования отчетов и личной странички студента.
 * 
 * @author Fedor Karpov
 * @version 1.1
 * @param idViolations       - идентификатор нарушения
 * @param idResident         - идентификатор насельника
 * @param idPeriod           - идентификатор периода в который произошла
 *                           активность
 * @param dateNegativeAct    - дата отрицательной активности
 * @param result             - итог по ситуации
 * @param commentNegativeAct - комментарий к отрицательной активности
 *                           (подробности, что сделал и т.д.)
 * @param user               - пользователь системы, который ввел нарушение
 */

@Entity
public class NegativeActivity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idNegativeAct;

	private Long idResident;
	private Long idPeriod;
	private LocalDate dateNegativeAct;
	private String result;
	private String commentNegativeAct;
	@Column(name = "user_b")
	private String user;

	@ManyToOne
	private Periods periods;

	public NegativeActivity() {
	}

	public NegativeActivity(Long idViolations, Long idResident, Long idPeriod, LocalDate dateNegativeAct, String result,
			String commentNegativeAct, String user) {
		super();
		this.idNegativeAct = idViolations;
		this.idResident = idResident;
		this.idPeriod = idPeriod;
		this.dateNegativeAct = dateNegativeAct;
		this.result = result;
		this.commentNegativeAct = commentNegativeAct;
		this.user = user;
	}

	public Long getIdViolations() {
		return idNegativeAct;
	}

	public void setIdViolations(Long idViolations) {
		this.idNegativeAct = idViolations;
	}

	public Long getIdResident() {
		return idResident;
	}

	public void setIdResident(Long idResident) {
		this.idResident = idResident;
	}

	public Long getIdPeriod() {
		return idPeriod;
	}

	public void setIdPeriod(Long idPeriod) {
		this.idPeriod = idPeriod;
	}

	public LocalDate getDateNegativeAct() {
		return dateNegativeAct;
	}

	public void setDateNegativeAct(LocalDate dateNegativeAct) {
		this.dateNegativeAct = dateNegativeAct;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getCommentNegativeAct() {
		return commentNegativeAct;
	}

	public void setCommentNegativeAct(String commentNegativeAct) {
		this.commentNegativeAct = commentNegativeAct;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
}