package ru.otus.mvcsecurity.repostory;

import org.springframework.data.repository.CrudRepository;

import ru.otus.mvcsecurity.domain.Violations;

public interface ViolationsRepository extends CrudRepository<Violations, Long> {

}
