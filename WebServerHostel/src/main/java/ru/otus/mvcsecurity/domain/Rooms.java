package ru.otus.mvcsecurity.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Класс "Комнаты" необходим для фиксации информации о комнате насельников
 * 
 * @author Fedor Karpov
 * @version 1.0
 * @param idRoom      - идентификатор комнаты
 * @param contentRoom - вместимость комнаты
 * @param numberRoom  - номер комнаты
 * @param attachment  - принадлежность либо мужской либо женской стороне
 * 
 */
@Entity
public class Rooms {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idRoom;

	private Integer contentRoom;
	private Integer numberRoom;
	private boolean attachment;

	public Rooms() {

	}

	public Rooms(Long idRoom, Integer contentRoom, Integer numberRoom) {
		super();
		this.idRoom = idRoom;
		this.contentRoom = contentRoom;
		this.numberRoom = numberRoom;
	}

	public Long getIdRoom() {
		return idRoom;
	}

	public void setIdRoom(Long idRoom) {
		this.idRoom = idRoom;
	}

	public Integer getContentRoom() {
		return contentRoom;
	}

	public void setContentRoom(Integer contentRoom) {
		this.contentRoom = contentRoom;
	}

	public Integer getNumberRoom() {
		return numberRoom;
	}

	public void setNumberRoom(Integer numberRoom) {
		this.numberRoom = numberRoom;
	}

	public boolean isAttachment() {
		return attachment;
	}

	public void setAttachment(boolean attachment) {
		this.attachment = attachment;
	}

}
