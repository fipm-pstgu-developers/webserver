package ru.otus.mvcsecurity.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

/**
 * Класс "Календарь присутствия" необходим для подсистемы фиксации насельника в
 * общежитии. Также фиксирует опаздания студента.
 * 
 * @author Fedor Karpov
 * @version 2.0
 * @param idPresenceCalendar - идентификатор календаря
 * @param idResident         - идентификатор насельника
 * @param dateAbsent         - дата отсутствия
 * @param weekends           - Выходные/праздники
 * @param address            - адрес по которому уедет насельник
 * @param reasonAbsence      - причина опоздания
 * @param startDateWeek      - дата заведения календаря
 * @param timeDelay          - дата и время опоздания
 * @param petitionUsed       - показатель использованного прошения. Необходимо
 *                           для показа на личной страничке студента когда тот
 *                           отсутствовал/опоздал по прошению
 */

@Entity
public class PresenceCalendar {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idPresenceCalendar;

	private Long idResident;
//	private LocalDate startDateAbsent;
//	private LocalDate endDateAbsent;
	private LocalDate dateAbsent;
	private boolean weekends;
	private String address;
	private LocalDate startDateWeek;
	private String reasonAbsence;
	private LocalDateTime timeDelay;
	private boolean petitionUsed;

	@ManyToOne
	private Resident resident;

	public PresenceCalendar() {
	}

	public PresenceCalendar(Long idPresenceCalendar, /* Long idResident, */
			/* LocalDate startDateAbsent, LocalDate endDateAbsent */ boolean weekends, String address,
			LocalDate startDateWeek, String reasonAbsence, LocalDateTime timeDelay, LocalDate dateAbsent,
			boolean petitionUsed) {
		super();
		this.idPresenceCalendar = idPresenceCalendar;
//		this.idResident = idResident;
//		this.startDateAbsent = startDateAbsent;
//		this.endDateAbsent = endDateAbsent;
		this.weekends = weekends;
		this.address = address;
		this.startDateWeek = startDateWeek;
		this.reasonAbsence = reasonAbsence;
		this.timeDelay = timeDelay;
		this.dateAbsent = dateAbsent;
		this.petitionUsed = petitionUsed;
	}

	public LocalDate getDateAbsent() {
		return dateAbsent;
	}

	public void setDateAbsent(LocalDate dateAbsent) {
		this.dateAbsent = dateAbsent;
	}

	public boolean isPetitionUsed() {
		return petitionUsed;
	}

	public void setPetitionUsed(boolean petitionUsed) {
		this.petitionUsed = petitionUsed;
	}

	public String getReasonAbsence() {
		return reasonAbsence;
	}

	public void setReasonAbsence(String reasonAbsence) {
		this.reasonAbsence = reasonAbsence;
	}

	public Long getIdPresenceCalendar() {
		return idPresenceCalendar;
	}

	public void setIdPresenceCalendar(Long idPresenceCalendar) {
		this.idPresenceCalendar = idPresenceCalendar;
	}

	public Long getIdResident() {
		return idResident;
	}

	public void setIdResident(Long idResident) {
		this.idResident = idResident;
	}

//	public LocalDate getStartDateAbsent() {
//		return startDateAbsent;
//	}
//
//	public void setStartDateAbsent(LocalDate startDateAbsent) {
//		this.startDateAbsent = startDateAbsent;
//	}
//
//	public LocalDate getEndDateAbsent() {
//		return endDateAbsent;
//	}
//
//	public void setEndDateAbsent(LocalDate endDateAbsent) {
//		this.endDateAbsent = endDateAbsent;
//	}

	public boolean isWeekends() {
		return weekends;
	}

	public void setWeekends(boolean weekends) {
		this.weekends = weekends;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public LocalDate getStartDateWeek() {
		return startDateWeek;
	}

	public void setStartDateWeek(LocalDate startDateWeek) {
		this.startDateWeek = startDateWeek;
	}

	public LocalDateTime getTimeDelay() {
		return timeDelay;
	}

	public void setTimeDelay(LocalDateTime timeDelay) {
		this.timeDelay = timeDelay;
	}

}