package ru.otus.mvcsecurity.domain;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Класс "Уборка комнат" необходим для фиксирования оценки уборки комнаты за
 * насельником.
 * 
 * @author Fedor Karpov
 * @version 1.0
 * @param idResident      - идентификатор насельника
 * @param idRoom          - идентификатор комнаты
 * @param idCleaningRoom  - идентификатор уборки
 * @param dateRating      - дата уборки
 * @param rating          - оценка
 * @param commentCleaning - комментарий к уборке
 */

@Entity
public class CleaningRooms {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idCleaningRoom;

	private Long idRoom;
	private Long idResident;
	private LocalDate dateRating;
	private Integer rating;
	private String commentCleaning;

	public CleaningRooms() {

	}

	public CleaningRooms(Long idCleaningRoom, Long idRoom, Long idResident, LocalDate dateRating, Integer rating,
			String commentCleaning) {
		super();
		this.idCleaningRoom = idCleaningRoom;
		this.idRoom = idRoom;
		this.idResident = idResident;
		this.dateRating = dateRating;
		this.rating = rating;
		this.commentCleaning = commentCleaning;
	}

	public Long getIdCleaningRoom() {
		return idCleaningRoom;
	}

	public void setIdCleaningRoom(Long idCleaningRoom) {
		this.idCleaningRoom = idCleaningRoom;
	}

	public Long getIdRoom() {
		return idRoom;
	}

	public void setIdRoom(Long idRoom) {
		this.idRoom = idRoom;
	}

	public Long getIdResident() {
		return idResident;
	}

	public void setIdResident(Long idResident) {
		this.idResident = idResident;
	}

	public LocalDate getDateRating() {
		return dateRating;
	}

	public void setDateRating(LocalDate dateRating) {
		this.dateRating = dateRating;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public String getCommentCleaning() {
		return commentCleaning;
	}

	public void setCommentCleaning(String commentCleaning) {
		this.commentCleaning = commentCleaning;
	}

}
