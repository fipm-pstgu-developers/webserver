package ru.otus.mvcsecurity.repostory;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ru.otus.mvcsecurity.domain.Rooms;

@Repository
public interface RoomsRepository extends CrudRepository<Rooms, Long> {
	/**
	 * Find room in the DB
	 * 
	 * @param numberRooms
	 * @return true or false
	 */	
	Optional<Rooms> findByNumberRoom(Integer numberRoom);
}
