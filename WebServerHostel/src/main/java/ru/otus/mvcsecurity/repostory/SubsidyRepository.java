/**
 * 
 */
package ru.otus.mvcsecurity.repostory;

import org.springframework.data.repository.CrudRepository;

import ru.otus.mvcsecurity.domain.Subsidy;

/**
 * @author Fedor Karpov
 *
 */
public interface SubsidyRepository extends CrudRepository<Subsidy, Long> {

	
}
