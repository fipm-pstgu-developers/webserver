package ru.otus.mvcsecurity.rest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ru.otus.mvcsecurity.domain.PresenceCalendar;
import ru.otus.mvcsecurity.domain.Resident;
import ru.otus.mvcsecurity.domain.Rooms;
import ru.otus.mvcsecurity.domain.Violations;
import ru.otus.mvcsecurity.repostory.HolidaysRepository;
import ru.otus.mvcsecurity.repostory.PeriodsRepository;
import ru.otus.mvcsecurity.repostory.PresenceCalendarRepository;
import ru.otus.mvcsecurity.repostory.ResidentRepository;
import ru.otus.mvcsecurity.repostory.RoomsRepository;
import ru.otus.mvcsecurity.repostory.ViolationsRepository;

@Controller
public class RoomsController {
	@Autowired
	private ResidentRepository resRepo;

	@Autowired
	private ViolationsRepository vioRepo;

	@Autowired
	private PeriodsRepository perRepo;

	@Autowired
	private RoomsRepository roomRepo;

	/**
	 * Class that accommodates entities Room and Resident
	 * 
	 * @author feodor
	 */
	public class StudentRoom {

		public Long roomId;
		public Long residentId;

		public String fioStudent;
		public Integer roomNumber;
		public String attachmentRoom;
		public Integer roomContent;

		public StudentRoom() {

		}

	}

	/**
	 * Метод необходим для показа всех комнат в БД.
	 * 
	 * @param model - модель для связывания контроллера и шаблона
	 * @return table_rooms - шаблон для отрисовки всех комнат из БД
	 */
	@GetMapping("/rooms/all")
	public String getAllRooms(Model model) {

		ArrayList<StudentRoom> all = new ArrayList<StudentRoom>();
		Resident resident;
		Rooms rooms;

		List<Resident> r = (List<Resident>) resRepo.findAll();
//		List<Rooms> allRooms = (List<Rooms>) roomRepo.findAll();
		System.out.println("rooms: " + r);

		for (Resident res : r) {

			StudentRoom stud = new StudentRoom();
//			TODO Предусмотреть вариант, когда в одной комнате несколько человек и прописать его.

//			Find the student by the room number of his residence
//			System.out.println("getRoomNumber: " + res.getRoomNumber());
//			resident = (resRepo.getByRoomNumber(rooms.getNumberRoom())).get();
			rooms = (roomRepo.findByNumberRoom(res.getRoomNumber())).get();

//			System.out.println("rooms: " + rooms);
//			System.out.println("resident: " + res.getFio());
			stud.residentId = res.getIdResident();
			stud.roomId = rooms.getIdRoom();
			stud.fioStudent = res.getFio();
			stud.roomNumber = rooms.getNumberRoom();
			stud.roomContent = rooms.getContentRoom();
			if (rooms.isAttachment())
				stud.attachmentRoom = ("Мужская");
			else
				stud.attachmentRoom = ("Женская");
//			System.out.println("stud: " + stud);
			all.add(stud);
//			System.out.println("stud: " + stud);
		}

		model.addAttribute("all", all);
		return "table_rooms";
	}

	/**
	 * Метод добавляет новые комнаты в комнатный фонд (или переделывает им
	 * принадлежность? - пока не сделано)
	 * 
	 * @param model
	 * @return table_rooms
	 */

	@GetMapping("/rooms/set")
	public String setFormRooms() {
		return "editing_room";
	}

	@PostMapping("/rooms/set")
	public String setRooms(@RequestParam String numberRoom, @RequestParam boolean number,
			@RequestParam String contentRoom, Model model) {
		/**
		 * Check if the room is already in the DB
		 */
		if ((roomRepo.findByNumberRoom(Integer.valueOf(numberRoom))).isPresent()) {

//			TODO - algorithm that edits existings rooms

		} else {
			Rooms room = new Rooms();
			room.setNumberRoom(Integer.valueOf(numberRoom));
			room.setContentRoom(Integer.valueOf(contentRoom));
			room.setAttachment(number);
			roomRepo.save(room);
			model.addAttribute("r", room);
		}

		return "editing_room";
	}

	// TODO
	// Создать таблицу с комнатами для контроля их в обшежитии: сколько мест в
	// комнате,
	// принадлежность комнат какой стороне, номер комнаты
	//

//	/**
//	 * Форма заполнения оценок за уборку в комнатах.
//	 * 
//	 * @param model - контейнер для шаблона
//	 * @return шаблон для отрисовки формы с заполненными насельниками
//	 */
//	@GetMapping("/orderRooms")
//	public String getFormOrderRooms(Model model) {
//		List<Resident> r = (List<Resident>) resRepo.findAll();
//		model.addAttribute("residentList", r);
//		return "order_rooms";
//	}
//
//	@PostMapping("/orderRooms")
//	public String viewOrderRooms(@RequestParam String dateWeek, @RequestParam("student") String[] student,
//			Model model) {
//
//		LocalDate dateW = LocalDate.parse(dateWeek);
//		PresenceCalendar prCalendar;
//		ArrayList<Long> residentListId = new ArrayList<>();
//		Violations vio;
//		ArrayList<LocalDateTime> dateDelay= new ArrayList<>();
//			
//		for (String stud : student) {
//			System.out.println("stud: " + stud);
//		}
////		int i = 0;
////		while (i < student.length) {
////			if (i + 1 < student.length) {
////				if (student[i + 1] != null) {
////					System.out.println("student[i]" + i + student[i]);
////					residentListId.add(Integer.toUnsignedLong(Integer.parseInt(student[i])));
////					
////				} else {
////					i++;
////				}
////			}
////			i++;
////		}
////		;
////		;
//
//		return "redirect:/orderRooms";
//	}

//	}
}
