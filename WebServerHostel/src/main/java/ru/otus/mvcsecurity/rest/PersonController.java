package ru.otus.mvcsecurity.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.otus.mvcsecurity.domain.PresenceCalendar;
import ru.otus.mvcsecurity.repostory.PresenceCalendarRepository;

import java.util.List;

@Controller
public class PersonController {

    private final PresenceCalendarRepository repository;

    @Autowired
    public PersonController(PresenceCalendarRepository repository) {
        this.repository = repository;
    }

//    @GetMapping("/list") //путь в скобках обозначается полный адрес по которому лежит шаблон
//    public String listPage(Model model) {
//        List<PresenceCalendar> persons = repository.findAll();
//        model.addAttribute("persons", persons);
//        return "list";
//    }
//
//    @GetMapping("/person")
//    public String personPage(
//    	@RequestParam("id") int id, Model model) {
//        PresenceCalendar person = repository.findById(id).orElseThrow(NotFoundException::new);
//        model.addAttribute("person", person);
//        return "person";
//    }
}
