/**
 * 
 */
package ru.otus.mvcsecurity.repostory;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.data.repository.CrudRepository;

import ru.otus.mvcsecurity.domain.Periods;
import ru.otus.mvcsecurity.domain.Resident;

/**
 * @author feodor
 *
 */
public interface PeriodsRepository extends CrudRepository<Periods, Long> {

	Optional<Periods> findByNamePeriodAndStartDatePeriodAndEndDatePeriod(String namePeriod, LocalDate startDatePeriod,
			LocalDate endDatePeriod);
	 
//	public Periods gettingIntoPeriod(LocalDate dateW);
	
		
	
}
